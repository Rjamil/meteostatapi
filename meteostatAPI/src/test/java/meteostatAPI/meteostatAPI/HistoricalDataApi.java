package meteostatAPI.meteostatAPI;

import static io.restassured.RestAssured.given;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;

import javax.swing.JOptionPane;

import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import com.google.common.base.Splitter;

import io.restassured.response.Response;

public class HistoricalDataApi {

//	Api documentation at https://api.meteostat.net/#history
//	You can make 200 calls to Meteostat api an hour.
//	We are making 2 api calls when we are calling getWeatherData(), so that makes it 100 calls and hour.

//	API key for Meteostat api
	String apiKey = "DDkqIgA1";

	String startDate = "2020-01-22"; // -- YYYY-MM-DD
	String endDate = "2020-03-20"; // -- YYYY-MM-DD
	
//	file name that is created for temperature data
	String temperatureFile = "temperature.csv";

	@Test
	public void getWeatherData() throws ParseException, InterruptedException {

//		Loop count is set to the testCaseId number in geoLocation.csv
		for (int a = 0; a < 463; a++) {

			String loopCount = Integer.toString(a);

			System.out.println(loopCount);

			String Latitude = geGeoLocation("geoLocation.csv", loopCount).get("Lat");
			String Longitude = geGeoLocation("geoLocation.csv", loopCount).get("Long");

			System.out.println("Lat = " + Latitude + "  Longitude = " + Longitude);

//		TRest call to get the station number for the latitude and longitude from geoLocation.csv for each testCaseId number
			Response res = given().

					when().get("https://api.meteostat.net/v1/stations/nearby?lat=" + Latitude + "&lon=" + Longitude
							+ "&limit=5&key=" + apiKey)
					.then().assertThat().statusCode(200).and().extract().response();

//		size if the data response for the station number
			int idsize = res.jsonPath().get("data.id.size()");

//		looping through all station numbers and calling the api to get weather data
			for (int i = 0; i < idsize; i++) {

//			json path to station number 
				String station = res.jsonPath().getString("data.id[" + i + "]");
//			json path to location name
				String name = res.jsonPath().getString("data.name[" + i + "]");
//			json path to distance of weather station from latitude and longitute 
				String distance = res.jsonPath().getString("data.distance[" + i + "]");

//			Rest call to get weather data with station number
				Response res1 = given().

						when()
						.get("https://api.meteostat.net/v1/history/daily?station=" + station + "&start=" + startDate
								+ "&end=" + endDate + "&key=" + apiKey)
						.then().assertThat().statusCode(200).and().extract().response();

//			if temperature data is not null for the station number being used to make the call
				if (!(res1.jsonPath().getString("data.temperature[0]") == null)) {

					System.out.println("data is not null");
					System.out.println("Station no : " + station + " temperrature = "
							+ res1.jsonPath().getString("data.temperature").replace("[", "").replace("]", ""));

					savedDataInCsv("testCaseId=" + loopCount, Latitude, Longitude, name.replace(",", " "), distance,
							station, res1.jsonPath().getString("data.temperature").replace("[", "").replace("]", ""),
							temperatureFile);
				}
//			if temperature data is null for the station number being used to make the call 
				else {

					System.out.println("data is null");
					System.out.println(
							"testCaseId=" + loopCount + "  " + Latitude + "  " + Longitude + "N/A N/A N/A N/A");
					savedDataInCsv("testCaseId=" + loopCount, Latitude, Longitude, "N/A", "N/A", "N/A", "N/A",
							temperatureFile);
				}

				break;
			}

//		if data size is zero when looking for station number and no station number is found
			if (idsize == 0) {
				System.out.println("Station not found!");
				System.out.println("testCaseId=" + loopCount + "  " + Latitude + "  " + Longitude + "N/A N/A N/A N/A");
				savedDataInCsv("testCaseId=" + loopCount, Latitude, Longitude, "N/A", "N/A", "N/A", "N/A",
						temperatureFile);

			}

		}
	}

// method used for storing data to csv file
	public void savedDataInCsv(String testCaseId, String Latitude, String Longitude, String name, String distance,
			String station, String temperature, String fileName) {

		try {

			FileWriter fw = new FileWriter(fileName, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			pw.println(testCaseId + "," + Latitude + "," + Longitude + "," + name + "," + distance + "," + station + ","
					+ temperature);
			pw.flush();
			pw.close();

//			JOptionPane.showMessageDialog(null,"Record saved");

		} catch (Exception e) {
			// TODO: handle exception
			JOptionPane.showMessageDialog(null, "Record not saved");
		}

	}

//	method used to get data from csv file
	private Map<String, String> geGeoLocation(String fileName, String testCaseId) {
//          Path filePath = null;
		Map<String, String> map = null;

		try {
//                filePath = Paths.get(getClass().getClassLoader()
//                            .getResource("dataproviders//qa.auto.preissue.agentValidationAPI//" + fileName).toURI());

			Scanner scanner = new Scanner(new File(System.getProperty("user.dir") + "\\" + fileName + ""));

			while (scanner.hasNext()) {
				map = Splitter.on(",").trimResults().withKeyValueSeparator("=").split(scanner.nextLine());

				if (map.get("testCaseId").trim().equals(testCaseId)) {
					break;
				}
			}

			scanner.close();

		} catch (Exception e) {
			System.out.println("Exception while reading the file " + e.getMessage());
			e.printStackTrace();
		}

		return map;
	}

}
